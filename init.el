;;清华插件源
(setq package-archives '(("gnu"   . "http://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
                         ("melpa" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/")))
;;递归扫描插件目录
(defun add-subdirs-to-load-path (dir)
  "Recursive add directories to 'load-path'."
  (let ((default-directory (file-name-as-directory dir)))
    (add-to-list 'load-path dir)
   (normal-top-level-add-subdirs-to-load-path)))
(add-subdirs-to-load-path "~/.emacs.d/my-elisp")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;主题
(require 'klere-theme)
(load-theme 'klere t)
;;
(global-hl-line-mode 1)
;;(set-face-background 'hl-line "#222222")
;;(set-face-foreground 'highlight nil)
;;光标颜色
(set-cursor-color "#FF3333")
(blink-cursor-mode 0) ;;禁用光标闪烁
(set-face-attribute
 'default nil
 :font (font-spec :name "-APPL-Monaco-normal-normal-normal-*-*-*-*-*-*-0-iso10646-1"
                  :weight 'normal
                  :slant 'normal
                  :size 14))
(dolist (charset '(kana han symbol cjk-misc bopomofo))
  (set-fontset-font
   (frame-parameter nil 'font)
   charset
   (font-spec :name "-ZYEC-楷体-normal-normal-normal-*-*-*-*-*-d-0-iso10646-1"
              :weight 'normal
              :slant 'normal
              :size 16)))
;;关闭工具栏、菜单栏、滚动条、启动动画
(tool-bar-mode 0)
(menu-bar-mode 0)
(scroll-bar-mode 0)
(setq inhibit-startup-message 1)
(setq transient-mark-mode 1)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;基本设置
;;F11全屏
(defun fullscreen ()
      (interactive)
      (set-frame-parameter nil 'fullscreen
                           (if (frame-parameter nil 'fullscreen) nil 'fullboth)))
(fullscreen)
(global-set-key (kbd "C-=") 'fullscreen)                                       ;;打开全屏
;; 快速打开配置文件
(defun open-init-file()
  (interactive)
  (find-file "~/.emacs.d/init.el"))
(global-set-key (kbd "C-0") 'open-init-file)     ;;F10打开配置文件
(defun open-init-file2()
  (interactive)
  (find-file "~/mm/hengyuancao.github.io/src/index.org"))
(global-set-key (kbd "C-9") 'open-init-file2)     ;;F9打开配置文件
(delete-selection-mode 1)                          ;;选中替换
(setq visible-bell t)                              ;;关闭提示音
(setq mouse-yank-at-point t)                       ;;黏贴到光标处
(fset 'yes-or-no-p 'y-or-n-p)                      ;;设置问答提示为 y-or-n,而不是yes-or-no
(setq make-backup-files nil)                       ;;不生成备份文件
(setq scroll-margin 3                              ;;防止页面滚动时跳动，scroll-margin 3
      scroll-conservatively 10000)
(show-paren-mode t)                                ;;括号匹配时显示另外一边的括号，而不是跳到另一个括号
(setq show-paren-style 'parentheses)
(show-paren-mode t)                                ;;显示匹配的括号
(setq default-directory "~/mm")              ;;设置默认工作目录
(setq kill-ring-max 100)                           ;;加大kill ring，防止出错后无法回滚文档
(add-hook 'org-mode-hook (lambda () (setq truncate-lines nil))) ;;自动换行
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;插件
;;自动保存
(require 'init-auto-save)
;;快速切换窗口
(require 'window-numbering)
(window-numbering-mode t)
;;avy跳跳跳
(require 'avy)
(global-set-key (kbd "C-:") 'avy-goto-char)
(global-set-key (kbd "C-'") 'avy-goto-char-2)
(global-set-key (kbd "M-g f") 'avy-goto-line)
;;主导窗口
(require 'ace-window)
(global-set-key (kbd "M-o") 'ace-window)
;;图标插件
(require 'all-the-icons)
;;目录树
(require 'neotree)
(global-set-key (kbd "C-8") 'neotree-toggle)
(setq neo-theme (if (display-graphic-p) 'icons 'arrow))
;;ivy
(require 'ivy)
(require 'swiper)
(require 'counsel)
(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(setq enable-recursive-minibuffers t)
(global-set-key "\C-s" 'swiper)
(global-set-key (kbd "C-c C-r") 'ivy-resume)
(global-set-key (kbd "<f6>") 'ivy-resume)
(global-set-key (kbd "M-x") 'counsel-M-x)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "<f1> f") 'counsel-describe-function)
(global-set-key (kbd "<f1> v") 'counsel-describe-variable)
(global-set-key (kbd "<f1> l") 'counsel-find-library)
(global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
(global-set-key (kbd "<f2> u") 'counsel-unicode-char)
(global-set-key (kbd "C-c g") 'counsel-git)
(global-set-key (kbd "C-c j") 'counsel-git-grep)
(global-set-key (kbd "C-c k") 'counsel-ag)
(global-set-key (kbd "C-x l") 'counsel-locate)
(global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
(define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)
;;smex
(require 'smex)
(smex-initialize)
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)
;;字体设置
(require 'cnfonts)
;;让编程更专注(lazycat)
;;(require 'awesome-tray)
;;(awesome-tray-mode 1)
;;company
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)
(require 'alpha-window)
(global-set-key [(f7)] 'loop-alpha)
(require 'aweshell)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;org-mode
(require 'org);;org-mode
(require 'org-bullets)
(add-hook 'org-mode-hook 'org-bullets-mode)
(require 'htmlize)
;;;;;;;;;;;博客
(require 'ox-publish)
(setq org-publish-project-alist
      '(
        ("blog-notes"
         :base-directory "~/mm/hengyuancao.github.io/src"
         :base-extension "org"
         :publishing-directory "~/mm/hengyuancao.github.io/html"
         :recursive t
;;         :publishing-function org-publish-org-to-html ;;org 7.X
         :publishing-function org-html-publish-to-html
;;         :link-home "index.html"
;;         :link-up "sitemap.html"
         :html-link-home "index.html"
         :html-link-up "sitemap.html"
         :headline-levels 5
         :section-numbers nil
         :auto-preamble t
         :auto-sitemap t                ; Generate sitemap.org automagically...
         :sitemap-filename "sitemap.org"  ; ... call it sitemap.org (it's the default)...
         :sitemap-title "Sitemap"         ; ... with title 'Sitemap'.
         :author "曹恒源"
         :email "2949790509@qq.com"
;;         :style    "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/main.css\"/>"
         :html-head  "<link rel=\"stylesheet\" type=\"text/css\" href=\"~/mm/hengyuancao.github.io/src/themes/css/main.css>"
         :html-preamble "可以添加博客的头部"
         :html-postamble " 评论系统代码(disqus,多说等等)
    <p class=\"author\">Author: %a (%e)</p><p>Last Updated %d . Created by %c </p>"
         )
        ("blog-static"
         :base-directory "~/mm/hengyuancao.github.io/src"
         :base-extension "css\\|js\\|pdf\\|png\\|jpg\\|gif\\|mp3\\|ogg\\|swf"
         :publishing-directory "~/mm/hengyuancao.github.io/html"
         :recursive t
         :publishing-function org-publish-attachment
         )
        ("blog" :components ("blog-notes" "blog-static"))
	))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;python
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(company-anaconda yasnippet pyenv-mode memoize highlight-indentation find-file-in-project)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
