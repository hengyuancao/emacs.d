#+TITLE: python
#+AUTHOR:yuan
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="/home/yuan/.emacs.d/notes/data/org.css"/>
* python基础
  
* python进阶
** Python
*** urllib
#+BEGIN_SRC python
- urllib.parse.urlencode(query)
将query字典转换为url路径中的查询字符串

urllib.parse.parse_qs(qs)

将qs查询字符串格式数据转换为python的字典

urllib.request.urlopen(url, data=None)

发送http请求，如果data为None，发送GET请求，如果data不为None，发送POST请求

返回response响应对象，可以通过read()读取响应体数据，需要注意读取出的响应体数据为bytes类型
#+END_SRC
*** os
    #+BEGIN_SRC python
    #拼接路径
    os.path.join(path1, path2)

    #+END_SRC
*** celery
Celery::是一个基于分布式消息传递的开源异步任务队列或作业队列。虽然它支持调度，但它的重点是实时操作
